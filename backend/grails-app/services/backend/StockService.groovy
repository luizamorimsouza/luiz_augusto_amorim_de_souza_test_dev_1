package backend

import grails.gorm.transactions.Transactional
import java.util.Date
import java.util.Calendar

@Transactional
class StockService {

    def getStocks(String name, int numbersOfHoursUntilNow) {
        long timestamp = new Date().getTime();

        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR_OF_DAY, numbersOfHoursUntilNow * -1); 
            
        def totalRetrivied = 0;
        def companies = Company.findAllByName(name)
		
        companies.each { company ->
            def stocks = Stock.findAllByCompanyAndPriceDateGreaterThanEquals(company, c.getTime())
            stocks.each { stock ->
                println "${stock.price} - ${stock.priceDate} - ${stock.company.name}"
            }

            totalRetrivied += stocks.size()
        }

        println ''
        println "Number of quotes retrieved: ${totalRetrivied}"
        println 'Executed in ' + (new Date().getTime() - timestamp) + ' millisseconds'
    }
}
