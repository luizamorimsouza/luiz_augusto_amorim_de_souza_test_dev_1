package backend

import java.util.Date;
import java.util.Calendar;

class BootStrap {

    def init = { servletContext ->

        def companies = [
            new Company(name: 'Microsoft', segment: 'TI'),
            new Company(name: 'Google', segment: 'TI'),
            new Company(name: 'Amazon', segment: 'TI')
        ]

        companies.each {
            int days = 29; // TODO change to 29 before commit
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_YEAR, days * -1);
            
            while (days >= 0) {
                c.set(Calendar.HOUR_OF_DAY, 10);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                
                // 10 am to 6 pm has 8 hours, 8 hours has 480 minutes
                // 6:00 o'clock is not being taken into account
                int finalMin = 480;
                
                if (days == 0) {
                    // It's current day, we need to get finaMin based on current
                    // hour, if and only if, the current hour is lower than 6:00 pm
                    Calendar currentDate = Calendar.getInstance();
                    if (currentDate.get(Calendar.HOUR_OF_DAY) < 10) {
                        finalMin = 0;
                    } else if (currentDate.get(Calendar.HOUR_OF_DAY) < 18) {
                        finalMin = (currentDate.get(Calendar.HOUR_OF_DAY) - 10) * 60 + currentDate.get(Calendar.MINUTE) + 1;
                    }
                }

                for (int minute = 0; minute < finalMin; minute++) {
                    // Generating price between 110 and 120 and truncating to two decimal places
                    float price = (float) Math.floor((Math.random() * (110 - 120 + 1) + 110) * 100) / 100;

                    def stock = new Stock(price: price, priceDate: c.getTime());
                    it.addToStocks(stock);

                    c.add(Calendar.MINUTE, 1);
                }

                days--;
                c.add(Calendar.DAY_OF_YEAR, 1);
            }

            it.save();
        }

        println "That's all folks!";
    }
    def destroy = {
    }
}
