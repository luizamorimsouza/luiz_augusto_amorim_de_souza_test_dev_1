package backend

import grails.gorm.transactions.Transactional
import java.lang.Math;

@Transactional
class CompanyService {

    def getCompanies() {
        return Company.list()
    }

    def getStandardDeviationFromCompanyStocks(Company company) {
        def stocks = Stock.findAllByCompany(company)
        double n = stocks.size(), sum = 0, mean;

        stocks.each { stock ->
            sum += stock.price;
        }

        mean = sum / n;
        sum=0;  

        stocks.each { stock ->
            sum += Math.pow((stock.price - mean), 2);
        }

        mean = sum / (n - 1);

        return Math.sqrt(mean);
    }
}
