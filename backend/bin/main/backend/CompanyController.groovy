package backend

import grails.converters.JSON

class CompanyController {

    CompanyService companyService
    StockService stockService

    def index() {
        render companyService.getCompanies().collect() { company ->
            [
                name: company.name,
                segment: company.segment,
                sd: companyService.getStandardDeviationFromCompanyStocks(company)
            ]
        } as JSON
    }

    def hello() {
        stockService.getStocks('Amazon', 30)
        render 'world!'
    }
}
