export interface Company {
    name: string;
    segment: string;
    sd: number;
}