import { Injectable } from '@angular/core';
import { Company } from '../models/company.mode';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  readonly BASE_URL = 'http://localhost:8080';

  constructor(
    private http: HttpClient
  ) { }

  public async companies(): Promise<Company[]> {
    try {
      return await this.http.get<Company[]>(`${this.BASE_URL}/company`).toPromise();
    } catch (e) {
      console.error(e);
      throw 'There was a problem trying to get the list of companies';
    }
  }
}
