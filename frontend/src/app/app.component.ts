import { Component } from '@angular/core';
import { Company } from './models/company.mode';
import { CompanyService } from './services/company.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  companies: Company[] = [];
  errorMsg: string;

  constructor (
    private companyService: CompanyService
  ) { }

  async loadCompanies(): Promise<void> {
    try {
      this.errorMsg = null;
      this.companies = await this.companyService.companies();
    } catch (e) {
      this.errorMsg = e;
    }
  }
}
