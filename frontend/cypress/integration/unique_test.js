import { cyan } from "color-name"
import { AssertionError } from "assert"

/*
Task #4
Write down a simple integration test to the task #3 you did before.
No need to check all the data retrieved by the button pushing. Just a Company name would be enough !
*/
describe('Task4', () => {
  it('push the button implemented on task #3 and shows the company names', () => {
      cy.visit('http://localhost:4200')
      cy.contains('Load Companies').click()
      cy.get('[id^=company_name_]').each(($td) => {
        cy.log($td.text());
      })
      expect(true).to.equal(true) //replace me !
  })
})
